package com.skynet.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.skynet.domain.User;

public interface UserRepository extends MongoRepository<User, Long> {
	
	User findByUsername(String username);

}
