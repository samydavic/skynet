package com.skynet.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/")
public class MainController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView getLoginPage(@RequestParam(value = "error", required = false) String error) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("login");
		if (error != null) {
			mv.addObject("error", "Invalid username or password!");
		}
		return mv;
	}
}
