package com.skynet.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.skynet.config.security.SpringSecurityConfig;

@Configuration
@Import({ SpringMongoConfig.class, SpringSecurityConfig.class})
@ComponentScan(basePackages = "com.skynet")
public class SpringRootConfig {

}
