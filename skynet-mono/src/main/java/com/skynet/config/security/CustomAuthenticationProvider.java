package com.skynet.config.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.skynet.dto.UserDto;
import com.skynet.enums.Role;
import com.skynet.services.UserService;

/**
 * @author samuel cuellar
 *
 */

@Component("customAuthenticationProvider")
public class CustomAuthenticationProvider implements AuthenticationProvider {

    public static final String GRANTED_AUTHORITY_ROLE_USER = "ROLE_USER";
    public static final String GRANTED_AUTHORITY_ROLE_ADMIN = "ROLE_ADMIN";

    protected static Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
    
    @Autowired
    UserService userService;

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        logger.debug("Performing custom authentication, user:{}", auth.getName());

        UserDto userDto = userService.findByUsername(auth.getName());
        if (userDto != null && auth.getCredentials().equals(userDto.getPassword())) {
            return new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), getAuthorities(userDto.getRole()));
        } else {
            throw new BadCredentialsException("Entered username and password are the same!");
        }
    }

    /**
     * Retrieves the correct ROLE type depending on the access level.
     * 
     * @param role
     * @return collection of granted authorities
     */
    public Collection<GrantedAuthority> getAuthorities(String role) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();

		if (Role.ADMIN.equals(role)) {
			authList.add(new SimpleGrantedAuthority(GRANTED_AUTHORITY_ROLE_ADMIN));
		} else {
			authList.add(new SimpleGrantedAuthority(GRANTED_AUTHORITY_ROLE_USER));
		}

        return authList;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
