package com.skynet.config.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.core.userdetails.User;

import com.skynet.repository.UserRepository;

public class AuthenticationEventListener implements ApplicationListener<AbstractAuthenticationEvent> {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationEventListener.class);

	@Autowired
	UserRepository userRepository;	

	@Override
	public void onApplicationEvent(AbstractAuthenticationEvent event) {
		logger.info("Authentication event: " + event.getSource());
		if (event.getAuthentication().isAuthenticated()) {
			String username;
			if (event.getAuthentication().getPrincipal() instanceof User) {
				User authUser = (User) event.getAuthentication().getPrincipal();
				username = authUser.getUsername();
			} else {
				username = event.getAuthentication().getPrincipal().toString();
			}
			logger.info("Logged User Name: {}", username);
		}
	}
}