var css = require("!css-loader!sass-loader!./app/assets/styles/app.scss");

module.exports = {
	module: {
	    loaders: [
	     	{
	        	test: /\.scss$/,
	        	loaders: ["style-loader", "css-loader", "sass-loader"]
	      	}
	    ]
	}
}