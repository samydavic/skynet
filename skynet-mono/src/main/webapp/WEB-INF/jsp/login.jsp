<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Login - Skynet</title>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/login.css" rel="stylesheet">
</head>
<body>
     
	<div class="container">
    	<div class="row vertical-offset-100">
	        <div class="col-md-4 col-md-offset-4">
	            <div class="panel panel-default">
	                <div class="panel-heading">
	                    <h3 class="panel-title">Welcome to Skynet</h3>
	                </div>
	                <div class="panel-body">

						<c:if test="${not empty error}">
							<div class="alert alert-danger">${error}</div>
						</c:if>
	                
	                    <form role="form" action="j_spring_security_check" method='POST'>
	                        <fieldset>
	                            <div class="form-group">
	                                <input class="form-control" placeholder="Username" name="username" autofocus>
	                            </div>
	                            <div class="form-group">
	                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
	                            </div>
	                            <!-- Change this to a button or input when using this as a form -->
	                            <input type="submit" value="Login" class="btn btn-success btn-block"/>
	                        </fieldset>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

</body>
</html>