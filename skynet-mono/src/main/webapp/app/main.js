window.Vue = require('vue');
window.VueRouter = require('vue-router');
window.VueResource = require('vue-resource');

import App from './App.vue';

const eventHub = new Vue();  
Vue.prototype.$eventHub = eventHub;  

const routes = [
  { path: '/', component: App }
];

Vue.use(VueRouter);

const router = new VueRouter({
  routes,
});

require('./components.js');

new Vue({
  el: '#app',
  router,
  eventHub
});

