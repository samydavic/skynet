Vue.component('navbar', require('./components/Navbar.vue'));
Vue.component('sidebar', require('./components/Sidebar.vue'));
Vue.component('datatable', require('./components/datatable.vue'));

Vue.component('users', require('./components/Users.vue'));
Vue.component('add-user', require('./components/AddUser.vue'));
Vue.component('roles', require('./components/Roles.vue'));
